// Adapted from: https://answers.unity.com/questions/867066/rotate-camera-around-moving-sphere.html

using UnityEngine;
using Photon.Pun;
using Photon.Realtime;

public class PlayerMovement : MonoBehaviourPunCallbacks, IPunObservable
{
    public bool isActive = true;
    public float acceleration = 200.0f;
    public float force = 10.0f;
    public Vector3 maxVelocity = new Vector3(10, 10, 10);
    public float jumpPower = 5.0f;
    public Rigidbody rigidBody;
    public bool enforceSpeedLimit = true;
    public bool canJump = false;

    private Player player;

    private float sphereRadius;

    private GameManager gameManager;

    /**
     * Network variables
     * These varialbles are only used if the current ball is not the one you
     * are controlling
     */
    #region Other Player Variables
    // The current position of the b4ll according to the server
    private Vector3 networkPosition;
    // The current rotation of the b4ll according to the server
    private Quaternion networkRotation;
    // The current velocity of the b4ll according to the server
    Vector3 networkVelocity;

    // After getting hit, turn off velocity control by the server for a bit,
    // until we receive an ACK
    private bool isServerControlling = true;
    // The timestamp that the ball resumes following server properties
    private float giveControlBackTime = .0f;
    // Duration in seconds that the ball is allowed to lose control to the
    // server
    [SerializeField]
    private const float giveControlBack = .6f;
    // The max strength in which the ball will make network positional corrections.
    // ! It has to be more than 1f, since the default strength is 1f.
    [SerializeField]
    private const float maxCorrectionStrength = 5f;
    // The maximum length in which the ball has to teleport to the correct position
    [SerializeField]
    private const float maxTeleportDist = 9f;
    private Vector3 lagPositionAdd = Vector3.zero;
    #endregion

    void Start()
    {
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        this.player = gameObject.GetComponent<Player>();
        rigidBody.maxAngularVelocity = 1000.0f;

        photonView.RefreshRpcMonoBehaviourCache(); // Cache RPC
        sphereRadius = transform.localScale.x;
    }

    void OnDrawGizmos() {
        if (!photonView.IsMine) {
            Gizmos.color = Color.red;
            Gizmos.DrawSphere(networkPosition + lagPositionAdd, 0.5f);
        }
    }


    void FixedUpdate()
    {
        // If it is connected, it needs to check whether we are moving only ourselves. If not, free move.
        if (isActive && (!PhotonNetwork.IsConnected || photonView.IsMine))
        {
            if (!gameManager.isGameEnded)
            {
                float horizontalMotion = -Input.GetAxis("Horizontal");
                float verticalMotion = Input.GetAxis("Vertical");
                // Accelerate(verticalMotion, horizontalMotion);

                if (Input.GetButton("Jump") && canJump)
                {
                    Jump();
                }
                Vector3 movement = (Input.GetAxis("Horizontal") * Camera.main.transform.right * force) + (Input.GetAxis("Vertical") * Camera.main.transform.forward * force);
                rigidBody.AddForce(movement, ForceMode.Force);
            }

            if (enforceSpeedLimit) EnforceSpeedLimit();

        } else if (PhotonNetwork.IsConnected && !photonView.IsMine) {
            // This is the other balls code
            // TODO: Should draw a holy full ray to the expected position. If there is an obstacle, teleport.
            // TODO: Don't use RPC. It's slow, send an event queue for the object affected
            // TODO: Maybe collision event only do for the affected guy?
            // TODO: Cannot rejoin after die
            // The difference in distance from the expected network position and
            // client ball position
            float errorDist = Vector3.Distance(rigidBody.position, networkPosition + lagPositionAdd);

            // Check if we are too far from the maximum allowed error distance
            // If so, teleport ourselves to the server location
            // (Solves respawning positioning too!)
            if (errorDist > maxTeleportDist) {
                transform.position = networkPosition;
            // } else if (Physics.SphereCast(new Ray(rigidBody.position, (networkPosition - rigidBody.position).normalized), sphereRadius/2f, maxTeleportDist)) {
            } else if (Physics.Linecast(rigidBody.position, networkPosition)) {
                // Check if there is any obstacle the size of the ball from the current position
                // To the target position. If there is any, teleport.
                transform.position = networkPosition;
                Debug.Log("Teleported player between balls");
            }

            // The scale of correction from (1 - maxCorrectionStrength) to
            // the expected position (Cubic ease out function)
            // TODO: Ease code not gewd
            // float corrStrength = MathUtil.cubicEaseOut(Mathf.Clamp(errorDist/maxTeleportDist, 0f, 1f)) * (maxCorrectionStrength - 1f) + 1f;
            float corrStrength = Mathf.Clamp(errorDist/maxTeleportDist, 0f, 1f) * (maxCorrectionStrength - 1f) + 1f;

            // Slowly correct client position to the server position with strength
            // depending on the distance plus smoothing on the lag. If the lag added
            // position is a wall or collision, don't add the lagg position
            rigidBody.MovePosition(
                Vector3.MoveTowards(
                    rigidBody.position,
                    networkPosition + (Physics.Linecast(networkPosition, networkPosition + lagPositionAdd) ? Vector3.zero : lagPositionAdd),
                    Time.fixedDeltaTime * corrStrength)
            );

            rigidBody.rotation = Quaternion.RotateTowards(rigidBody.rotation, networkRotation, Time.fixedDeltaTime * 100.0f);

            if (isServerControlling) {
                // TODO: Predictive movement
                rigidBody.velocity = networkVelocity;
            } else {
                if (Time.time >= giveControlBackTime) {
                    isServerControlling = true;
                    Debug.Log("Server velo Reenabled");
                }
            }
        }
    }

    void EnforceSpeedLimit()
    {
        // X
        if (rigidBody.angularVelocity.x > maxVelocity.x)
        {
            rigidBody.angularVelocity = new Vector3(maxVelocity.x, rigidBody.angularVelocity.y, rigidBody.angularVelocity.z);
        }
        else if (rigidBody.angularVelocity.x < maxVelocity.x * -1)
        {
            rigidBody.angularVelocity = new Vector3(maxVelocity.x * -1, rigidBody.angularVelocity.y, rigidBody.angularVelocity.z);
        }

        // Y
        if (rigidBody.angularVelocity.y > maxVelocity.y)
        {
            rigidBody.angularVelocity = new Vector3(rigidBody.angularVelocity.x, maxVelocity.y, rigidBody.angularVelocity.z);
        }
        else if (rigidBody.angularVelocity.y < maxVelocity.y * -1)
        {
            rigidBody.angularVelocity = new Vector3(rigidBody.angularVelocity.x, maxVelocity.y * -1, rigidBody.angularVelocity.z);
        }

        // Z
        if (rigidBody.angularVelocity.z > maxVelocity.z)
        {
            rigidBody.angularVelocity = new Vector3(rigidBody.angularVelocity.x, rigidBody.angularVelocity.y, maxVelocity.z);
        }
        else if (rigidBody.angularVelocity.z < maxVelocity.z * -1)
        {
            rigidBody.angularVelocity = new Vector3(rigidBody.angularVelocity.x, rigidBody.angularVelocity.y, maxVelocity.z * -1);
        }


    }

    void Accelerate(float forward, float sideways)
    {
        Vector3 torqueVector = new Vector3(forward, 0, sideways);
        rigidBody.AddTorque(torqueVector * acceleration, ForceMode.Acceleration);
    }

    void Jump()
    {
        Vector3 newVelocity = rigidBody.velocity;
        newVelocity.y = jumpPower;
        rigidBody.velocity = newVelocity;

        // Play audio
        if (this.player != null)
            this.player.audioJump.Play();
    }

    private void OnCollisionEnter(Collision collision)
    {
        GameObject obj = collision.gameObject;
        
        if (obj.tag == "Wall")
        {
            Debug.Log("Collided with wall");
            // Play wall collision audio
            this.player.audioWallCollision.Play();
        }
        else if (obj.tag == "Ramp")
        {
            Debug.Log("Collided with ramp");

            // Play ramp landing audio
            this.player.audioRampLanding.Play();
        } else if (obj.tag == "Player") {
            if (!PhotonNetwork.IsConnected || photonView.IsMine) {
                // TODO: Might have to disable this collision thing in the duration that serverVelocity is off
                // Tag to send collision result to the other player in the server

                // If collide, make the other ball lose server control for a bit
                PlayerMovement mvScript = obj.GetComponent<PlayerMovement>();
                mvScript.ballLoseServerControl(true);

                foreach (ContactPoint contact in collision.contacts) {
                    // Just draw collision rays
                    Debug.DrawRay(contact.point, contact.normal * 10, Color.white, 30f);
                    Debug.DrawRay(contact.point, (mvScript.rigidBody.velocity - mvScript.networkVelocity) * 5, Color.red, 30f);
                }

                // Sends a command that the other ball has been hit by a vector
                obj.GetComponent<PhotonView>().RpcSecure(
                    "gotHit",
                    obj.GetComponent<PhotonView>().Owner,
                    false,
                    (mvScript.rigidBody.velocity - mvScript.networkVelocity)
                );

                Debug.LogFormat("Sent hit! {0}", (mvScript.rigidBody.velocity - mvScript.networkVelocity));
                // Sends the RPC immediately
                PhotonNetwork.SendAllOutgoingCommands();
            }
        }
    }

    private void OnCollisionStay(Collision collision)
    {
        GameObject obj = collision.gameObject;

        if (obj.tag == "Ramp")
        {
            // Play ramp rolling audio if there is movement
            Rigidbody rb = gameObject.GetComponent<Rigidbody>();
            if (rb.velocity != Vector3.zero || rb.angularVelocity != Vector3.zero)
            {
                AudioSource rampRollingAudio = player.audioRampRoll;

                rampRollingAudio.volume = Utilities.getHighestVectorRatio(rb.angularVelocity, this.maxVelocity);

                if (!rampRollingAudio.isPlaying)
                {
                    rampRollingAudio.Play();
                }
            }
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        GameObject obj = collision.gameObject;

        if (obj.tag == "Ramp")
        {
            // Stop playing ramp rolling audio
            AudioSource rampRollingAudio = player.audioRampRoll;
            if (rampRollingAudio.isPlaying)
            {
                rampRollingAudio.Stop();
            }
        }
    }

    /**
     * This will be called to give up server's ball control and let the
     * ball's velocity be free. After a given time period, control is given back
     * to the server.
     */
    public void ballLoseServerControl(bool isGive) {
        if (isGive) {
            isServerControlling = false;
            giveControlBackTime = Time.time + giveControlBack;
        } else {
            isServerControlling = true;
        }
    }

    /*
     * Custom implementation of netcode
     */
    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info) {
        if (stream.IsWriting) {
            stream.SendNext(rigidBody.position);
            stream.SendNext(rigidBody.rotation);
            stream.SendNext(rigidBody.velocity);
            stream.SendNext(rigidBody.angularVelocity);
            stream.SendNext(player == null ? 3 : player.lives);
            stream.SendNext(player.name);
        } else {
            float lag = Mathf.Abs((float) (PhotonNetwork.Time - info.SentServerTime));

            networkPosition = (Vector3) stream.ReceiveNext();
            networkRotation = (Quaternion) stream.ReceiveNext();
            networkVelocity = (Vector3) stream.ReceiveNext();
            rigidBody.angularVelocity = (Vector3) stream.ReceiveNext();
            var a = stream.ReceiveNext();
            if (a != null)
                player.lives = (int) a;
            player.name = (string) stream.ReceiveNext();

            lagPositionAdd = rigidBody.velocity * lag;
        }
    }

    // Will be called if other balls hits our ball. The ball must apply a force
    // To itself because it got hit
    [PunRPC]
    void gotHit(Vector3 pushForce, PhotonMessageInfo info) {
        // Causes the local player to be pushed
        rigidBody.velocity += pushForce;
        photonView.RpcSecure("hitAck", info.Sender, false, true);
    }

    // Will be called if the ball that we hit acknowledged that it got hit
    [PunRPC]
    void hitAck(bool ack) {
        // TODO: Handle ACK
        if (ack) {
            // Give back server control
            ballLoseServerControl(false);
            Debug.Log("Hit acknowledged!");
        }
    }
}
