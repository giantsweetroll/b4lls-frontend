using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlowZone : MonoBehaviour
{
    [SerializeField] float massMultiplier = 10;
    [SerializeField]
    [Tooltip("Tell which game object to watch for intersection before this game object falls")]
    private GameObject _ground;
    private Rigidbody rigidBody;
    private Collider stickyCollider, groundCollider;

    private void Start()
    {
        rigidBody = gameObject.GetComponent<Rigidbody>();
        stickyCollider = gameObject.GetComponent<Collider>();

        if (_ground != null)
            groundCollider = _ground.GetComponent<Collider>();
    }

    private void Update()
    {
        if (groundCollider != null && !rigidBody.useGravity && !stickyCollider.bounds.Intersects(groundCollider.bounds))
        {
            // Enable gravity if sticky zone is not intersecting with Ground
            rigidBody.useGravity = true;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        GameObject obj = collision.gameObject;
 
        if (obj.tag == "Player")
        {
            Player player = obj.GetComponent<Player>();

            // Mark as in sticky zone
            player.isOnSticky = true;

            // Play sticky audio
            AudioSource stickyLandingAudio = player.audioStickyLanding;
            stickyLandingAudio.Play();

            // Change the mass of the player so it is harder to move (slower)
            // But this way, if the player gets hit they will be harder to knockback
            Rigidbody rigidBody = obj.GetComponent<Rigidbody>();
            rigidBody.velocity = Vector3.zero;
            rigidBody.angularVelocity = Vector3.zero;
            rigidBody.mass *= massMultiplier;
            //print(obj.GetComponent<Rigidbody>().mass);
        }
    }

    private void OnCollisionStay(Collision collision)
    {
        GameObject obj = collision.gameObject;

        if (obj.tag == "Player")
        {
            // Play sticky rolling audio as long as the ball is moving
            Player player = obj.GetComponent<Player>();
            PlayerMovement playerMovement = obj.GetComponent<PlayerMovement>();
            Rigidbody rb = obj.GetComponent<Rigidbody>();
            if (rb.velocity != Vector3.zero || rb.angularVelocity != Vector3.zero)
            {
                AudioSource stickyRollingAudio = player.audioStickyRoll;
                stickyRollingAudio.volume = Utilities.getHighestVectorRatio(rb.angularVelocity, playerMovement.maxVelocity);
                if (!stickyRollingAudio.isPlaying)
                {
                    stickyRollingAudio.Play();
                }
            }
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        GameObject obj = collision.gameObject;

        if (obj.tag == "Player")
        {
            // Return the mass to normal
            obj.GetComponent<Rigidbody>().mass /= massMultiplier;
            //print(obj.GetComponent<Rigidbody>().mass);

            Player player = obj.GetComponent<Player>();

            // Stop playing sticky rolling audio
            Rigidbody rb = obj.GetComponent<Rigidbody>();
            AudioSource stickyRollingAudio = player.audioStickyRoll;
            if (stickyRollingAudio.isPlaying)
            {
                stickyRollingAudio.Stop();
            }

            // Mark as not in sticky zone
            player.isOnSticky = false;
        }
    }
}
