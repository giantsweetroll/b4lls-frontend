using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloatingPlatformSpawner : MonoBehaviour
{
    [Tooltip("What object to spawn")]
    public GameObject objectToSpawn;

    [Tooltip("Offset of the spawned object relative to the game object")]
    public Vector3 offset;
    [Tooltip("Scale of the spawned object")]
    public Vector3 scale = new Vector3(0.05f, 0.2f, 0.05f);

    [Tooltip("Try to match the game object's rotation")]
    public bool followObjectOrientation = false;
    [Tooltip("Whether to make the game object")]
    public bool setGameObjectAsParent = false;

    // Start is called before the first frame update
    void Start()
    {
        GameObject clone = setGameObjectAsParent? Instantiate(objectToSpawn, gameObject.transform, false) : Instantiate(objectToSpawn);
        clone.transform.position = gameObject.transform.position + offset;
        clone.transform.localScale = scale;

        if (followObjectOrientation)
        {
            clone.transform.rotation = gameObject.transform.rotation;
        }
    }
}
