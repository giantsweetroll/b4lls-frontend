// From: https://answers.unity.com/questions/867066/rotate-camera-around-moving-sphere.html

using UnityEngine;

public class CameraControls : MonoBehaviour
{
    public bool isActive = true;

    public float turnSpeed = 4.0f;
    public Transform player;

    public float height = 1f;
    public float distance = -2f;

    private Vector3 offsetX;
    private Vector3 offsetY;

    void Start()
    {
        offsetX = new Vector3(0, height, distance);
        offsetY = new Vector3(0, 0, distance);
    }
    void LateUpdate()
    {
        if (isActive)
        {
            offsetX = Quaternion.AngleAxis(Input.GetAxis("Mouse X") * turnSpeed, Vector3.up) * offsetX;
            offsetY = Quaternion.AngleAxis(Input.GetAxis("Mouse X") * turnSpeed, Vector3.right) * offsetY;

            // Simple check to see if the player exists
            if (player != null)
            {
                transform.position = player.position + offsetX;
                transform.LookAt(player.position);
            }
        }
    }
}
