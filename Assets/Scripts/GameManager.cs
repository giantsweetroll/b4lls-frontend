using TMPro;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public bool isGameEnded = false;

    [SerializeField] private GameObject instructionText, pauseMenu, winnerDisplay;

    private CursorLockMode _cursorWantedMode;

    private bool menuActive = false;

    public static bool scriptEnabled = false;

    private void OnEnable()
    {
        _cursorWantedMode = CursorLockMode.Locked;
    }

    private void Update()
    {
        if (!isGameEnded)
        {
            if (pauseMenu != null)
            {
                menuActive = pauseMenu.activeSelf;
            }

            SetCursorState();
        }


        // TODO: Pretty evil
        if (scriptEnabled) {
            Player playerLast = null;
            int ct = 0;
            int i = 0;
            int amt = GameObject.FindGameObjectsWithTag("Player").Length - 2;
            Debug.LogFormat("NEED {0} TO LOSE", amt);
            foreach (GameObject obj in GameObject.FindGameObjectsWithTag("Player")) {
                if (i == 0) {
                    i++;
                    continue;
                }
                Player player = obj.GetComponent<Player>();
                if (player.lives <= 0) {
                    ct++;
                } else {
                    playerLast = player;
                }
                if (ct >= amt) {
                    scriptEnabled = false;
                    showWinnerDisplay(playerLast);
                    break;
                }
                i++;
        }}
    }
    private void Start()
    {
        Invoke("HideInstructions", 5);
    }

    // Apply requested cursor state
    private void SetCursorState()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            _cursorWantedMode = CursorLockMode.None;
            if (pauseMenu != null)
                pauseMenu.SetActive(!menuActive);
        }

        if (Input.GetMouseButtonDown(0) && !menuActive)
        {
            _cursorWantedMode = CursorLockMode.Locked;
        }

        if (isGameEnded)
        {
            _cursorWantedMode = CursorLockMode.None;
        }

        // Apply cursor state
        Cursor.lockState = _cursorWantedMode;
        // Hide cursor when locking
        Cursor.visible = (CursorLockMode.Locked != _cursorWantedMode);
    }

    private void HideInstructions()
    {
        if (instructionText != null)
            instructionText.SetActive(false);
    }

    public void showWinnerDisplay(Player winner)
    {
        if (winnerDisplay != null)
        {
            EndgamePanel endgamePanel = winnerDisplay.GetComponent<EndgamePanel>();

            endgamePanel.winningPlayer = winner;
            winnerDisplay.SetActive(true);
        }
        else
        {
            Debug.LogError("Winner Display is null");
        }
    }
}
