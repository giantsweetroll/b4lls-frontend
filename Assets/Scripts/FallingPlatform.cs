using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallingPlatform : MonoBehaviour
{
    [SerializeField] private int maxCounter = 100;
    [SerializeField] private float counterSpeed = 0.1f;
    [SerializeField] private float regenRate = 1;
    [SerializeField] private bool allowRegenBeforeGone = false;

    private float counter = 0;
    private bool enableCounter = false;
    private void Update()
    {
        if (enableCounter)
        {
            counter += counterSpeed;

            if (counter >= maxCounter)
            {
                gameObject.SetActive(false);
            }
        } else if (allowRegenBeforeGone && counter > 0)
        {
            counter -= regenRate;
            if (counter < 0) counter = 0;
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        GameObject obj = collision.gameObject;

        if (obj.tag == "Player")
        {
            enableCounter = true;
        }
    }
    private void OnCollisionExit(Collision collision)
    {
        GameObject obj = collision.gameObject;

        if (obj.tag == "Player")
        {
            enableCounter = false;
        }
    }
}
