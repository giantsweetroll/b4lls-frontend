using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using Photon.Realtime;
using TMPro;

public class PUNInitializer : MonoBehaviourPunCallbacks
{

    string gameVersion = "2";
    private string username = "";
    private string roomName = "a";

    [Tooltip("When creating a random room, the amount of words in the room string is this")]
    [SerializeField] private const int roomWordCount = 2;

    [Tooltip("Max players in a given room")]
    [SerializeField] private const int maxPlayers = 4;

    [Tooltip("Max players in a given room")]
    [SerializeField] private GameObject playerPrefab;

    // To be set up from UsernameDialog
    public bool shouldCreateRoom;

    /**
     * Joins a room. If the string is empty, creates a new room instead
     */
    private void JoinRoom()
    {
        PhotonNetwork.JoinOrCreateRoom(this.roomName,
            new RoomOptions { MaxPlayers = maxPlayers, IsVisible = false }, null);
    }
    
    public void SetUsername(TMP_InputField usernameInput) {
        // TODO: Check username validity
        string name = usernameInput.text;

        username = name;
    }

    public void JoinRoomInput(TMP_InputField roomInput) {
        if (username.Length < 4 || username.Contains(' ')) {
            return;
        }
        if (shouldCreateRoom) {
            Debug.Log("User clicked create room");
        } else {
            Debug.Log("User clicked join room");
        }
        ConnectNet(shouldCreateRoom ? "" : roomInput.text);
    }

    /**
     * Connects to the PUN networks with the specified room name. If 
     * the room name is empty, create a new room instead.
     * Returns the name of the new room that is being created / joined.
     */
    public void ConnectNet(string roomName)
    {
        PhotonNetwork.NickName = username;
        // If the roomName specified is empty, generate new string.
        if (string.IsNullOrEmpty(roomName))
        {
            this.roomName = WordsUtil.RandomWordCombination(roomWordCount).Trim();
            Debug.LogFormat("No room name is entered. Auto generating room `{0}`", this.roomName);
        } else
        {
            this.roomName = roomName;
        }

        if (PhotonNetwork.IsConnected)
        {
            JoinRoom();
        }
        else
        {
            PhotonNetwork.ConnectUsingSettings();
            PhotonNetwork.GameVersion = gameVersion;
        }
    }

    public void StartGame()
    {
        // Lock room
        // PhotonNetwork.CurrentRoom.IsOpen = false;
        PhotonNetwork.LoadLevel("WaitingRoom");
    }

    private void Awake()
    {
        PhotonNetwork.AutomaticallySyncScene = true;
        PhotonNetwork.UseRpcMonoBehaviourCache = true; // Sync RPC methods for all components
    }

    /*
     * Callback overrides
     */

    public override void OnConnectedToMaster()
    {
        JoinRoom();
    }

    public override void OnDisconnected(DisconnectCause cause)
    {
        Debug.LogWarningFormat("Disconnected from the PUN server. Cause: {0}", cause);
    }

    public override void OnJoinedRoom()
    {
        Debug.LogFormat("Room {0} successfully joined!", roomName);
        if (PhotonNetwork.IsMasterClient)
        {
            StartGame();
        }
    }

    public override void OnJoinRoomFailed(short returnCode, string message)
    {
        Debug.LogFormat("Joining room {0} error. Reason: {1}", roomName, message);
    }
}
