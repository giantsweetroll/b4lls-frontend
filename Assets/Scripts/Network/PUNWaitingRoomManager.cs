using Photon.Pun;
using Photon.Realtime;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class PUNWaitingRoomManager : MonoBehaviourPunCallbacks
{

    [SerializeField]
    private UsernameDisplay[] names;
    [SerializeField]
    private TMP_Text roomName;
    private string skinChoice;

    [SerializeField]
    private Button StartButton;

    void Start() {
        Debug.LogFormat("Username: {0} Room: {1}", PhotonNetwork.NickName, PhotonNetwork.CurrentRoom.Name);
        roomName.text = "Room: " + PhotonNetwork.CurrentRoom.Name;
        refreshPlayerList();

        if (!PhotonNetwork.IsMasterClient) {
            StartButton.enabled = false;
        }
    }

    private void refreshPlayerList() {
        int i = 0;
        Debug.Log(names.Length);
        foreach (var item in PhotonNetwork.CurrentRoom.Players) {
            Debug.Log(PhotonNetwork.CurrentRoom.Name);
            Debug.Log(item.Value.NickName);
            names[i].UpdatePlayer((item.Value.IsMasterClient ? "[HOST] " : "") + item.Value.NickName);
            i++;
        }
    }

    public void StartGame() {
        PhotonNetwork.LoadLevel("Scene1");
        if (PhotonNetwork.IsMasterClient) {
            PhotonNetwork.CurrentRoom.IsOpen = false;
        }
    }

    public override void OnPlayerEnteredRoom(Photon.Realtime.Player player) {
        Debug.LogFormat("Player Joined: {0}", player.NickName);
        refreshPlayerList();
    }

    public override void OnPlayerLeftRoom(Photon.Realtime.Player player) {
        Debug.LogFormat("Player Left: {0}", player.NickName);
        refreshPlayerList();
    }
}
