using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;

public class PunGameManager : MonoBehaviourPunCallbacks
{

    [Tooltip("Player prefab to instantiate when new players are coming")]
    [SerializeField] private GameObject playerPrefab;

    [Tooltip("Player prefab lowest ground pos. Will disappear if player goes below this")]
    [SerializeField] private GameObject playerLowestGroundPos;

    [Tooltip("List of respawn places for spawned b4ll")]
    [SerializeField] private List<Transform> respawnLocations;

    [Tooltip("Camera object")]
    [SerializeField] private GameObject mainCamera;

    public static GameObject localPlayer;

    public PlayerHPDisplay[] displays;
    // TODO: Evil
    public float waitTime = 0;
    public bool doYes = true;

    // Start is called before the first frame update
    void Start()
    {
        if (PhotonNetwork.IsConnectedAndReady)
        {
            Debug.LogFormat("Entering BALLARENA connected to PUN server with room `{0}`",
                PhotonNetwork.CurrentRoom.Name);
            
            // First time joining the room, as the client destroy all test balls
            foreach (GameObject obj in GameObject.FindGameObjectsWithTag("Player")) {
                if (obj.name == "Player_TEST") Destroy(obj);
            }

            // Instanstiate own player ball
            if (localPlayer == null)
            {
                /* ! IMPORTANT NOTE
                 * When a local player joins a game or a player joins a game,
                 * the other balls instances are instanstiated in based on the
                 * playerPrefab prefab. Unless specified otherwise, they will
                 * have the basic properties of that prefab.
                */
                localPlayer = PhotonNetwork.Instantiate(playerPrefab.name, new Vector3(0f, 30f, 0f), Quaternion.identity);

                // Sets the lowest ground pos
                Player pScript = localPlayer.GetComponent<Player>();
                pScript.lowestGroundPos = playerLowestGroundPos.transform;
                pScript.respawnLocations = respawnLocations;
                pScript.name = PhotonNetwork.NickName;

                // Setup camera follow
                mainCamera.GetComponent<CameraControls>().player = localPlayer.transform;
            }

            // TODO: Evil
            waitTime = Time.time;

        } else
        {
            Debug.Log("Entering BALLARENA NOT connected to PUN server");
        }
    }

    // Update is called once per frame
    void Update()
    {
        // TODO: Everythins pretty evil here
        if (doYes && Time.time > waitTime + 2) {
            Debug.Log("BRUHRBUHRBRUHRUHRR " + GameObject.FindGameObjectsWithTag("Player").Length);
            GameManager.scriptEnabled = true;

            int i = 0;
            foreach (GameObject obj in GameObject.FindGameObjectsWithTag("Player")) {
                if (i == 0) {
                    i++;
                    continue;
                }
                displays[i-1].player = obj.GetComponent<Player>();
                Debug.LogFormat("Players now: {0}", obj.GetComponent<Player>().name);
                displays[i-1].gameObject.SetActive(true);
                displays[i-1].UpdateLife();
                i++;
            }
            doYes = false;
        }
    }
}
