using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class SwitchCamera : MonoBehaviourPun
{
   public void Switch()
    {
        CameraControls camControl = gameObject.GetComponent<CameraControls>();
        FreeFlyCamera freeFlyCam = gameObject.GetComponent<FreeFlyCamera>();

        camControl.isActive = !camControl.isActive;

        bool activateFreeFly = !camControl.isActive;
        freeFlyCam.SetActive(activateFreeFly);
        if (activateFreeFly)
        {
            freeFlyCam.ResetPosition();
        }
    }
}
