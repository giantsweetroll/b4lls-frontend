using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateCamera : MonoBehaviour
{
    public GameObject target;
    public Vector3 offset;
    public float speedMultiplier = 1;

    private void Start()
    {
        gameObject.transform.position += offset;
    }

    private void Update()
    {
        transform.LookAt(target.transform);
        transform.Translate(Vector3.right * Time.deltaTime * speedMultiplier);
    }
}
