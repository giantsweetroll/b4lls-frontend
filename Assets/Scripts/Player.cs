using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class Player : MonoBehaviourPunCallbacks
{
    [SerializeField] private GameObject instructionsText, spectatingText;
    [SerializeField] private Rigidbody rigidBody;
    private bool isSpectating = false;

    public int lives = 3;
    public Color color = new(255, 255, 255);

    public static Sprite skin;

    public List<Transform> respawnLocations;
    public Transform lowestGroundPos;

    public bool lose = false;

    public AudioSource audioBounce,
        audioGroundRoll,
        audioPlayerCollision,
        audioStickyLanding,
        audioStickyRoll,
        audioJump,
        audioWallCollision,
        audioRampLanding,
        audioRampRoll,
        audioDeath,
        audioDeathPermanent;

    [HideInInspector] public bool isOnSticky = false;

    [SerializeField] private bool autoInstanstiateDirectioner = true;
    [SerializeField] private GameObject directionerPrefab;

    private GameManager gameManager;

    void Start() {
        // TODO: Evil
        color = Random.ColorHSV();
        
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        GameObject obj = Instantiate(directionerPrefab, transform.position, Quaternion.identity);
        PlayerDirection dirScript = obj.GetComponent<PlayerDirection>();
        dirScript.player = transform;
        dirScript.playerRigidBody = rigidBody;


        // TODO: Evil
        if (!photonView.IsMine) {
            skin = PlayerCustomization.spriteList[Random.Range(0, PlayerCustomization.spriteList.Count-1)];
        }

        if (skin != null)
        {
            var croppedTexture = new Texture2D((int)skin.rect.width, (int)skin.rect.height);
            var pixels = skin.texture.GetPixels((int)skin.textureRect.x,
                                                    (int)skin.textureRect.y,
                                                    (int)skin.textureRect.width,
                                                    (int)skin.textureRect.height);
            croppedTexture.SetPixels(pixels);
            croppedTexture.Apply();

            MeshRenderer renderer = gameObject.GetComponent<MeshRenderer>();

            renderer.material.mainTexture = croppedTexture;
        }
    }

    // Update is called once per frame
    void Update()
    {
        // ? FIXME: Temporary fix for checking outside of bounds in multiplayer.
        if ((!PhotonNetwork.IsConnected || photonView.IsMine) && (transform.position.y <= lowestGroundPos.position.y - 10 && lives > 0))
        {
            lives--;

            if (respawnLocations.Count > 0)
            {
                if (lives > 0)
                {
                    // Play death audio
                    if (!gameManager.isGameEnded)
                    {
                        audioDeath.Play();
                    }

                    // Respawn
                    int index = Random.Range(0, respawnLocations.Count);
                    respawn(respawnLocations[index].position);
                } 
                // Game over, no more lives
                else
                {
                    if (!gameManager.isGameEnded)
                    {
                        lose = true;
                        // Play game over audio
                        audioDeathPermanent.Play();

                        // If instructions text is still visible, hide it
                        if (instructionsText != null && instructionsText.activeSelf)
                        {
                            instructionsText.SetActive(false);
                        }

                        // Show spectating text
                        if (spectatingText != null)
                            spectatingText.SetActive(true);
                    } 

                    // Change to free fly mode (spectator)
                    GameObject mainCamera = GameObject.FindGameObjectWithTag("MainCamera");
                    if (mainCamera != null)
                    {
                        // Turn off physics
                        rigidBody.useGravity = false;
                        rigidBody.angularVelocity = Vector3.zero;
                        rigidBody.velocity = Vector3.zero;

                        // Disable movement
                        PlayerMovement pm = gameObject.GetComponent<PlayerMovement>();
                        pm.isActive = false;

                        // Move the player to somewhere and/or disable or make it invisible
                        gameObject.transform.position = new Vector3(9999, -999, 9999);

                        // Switch camera
                        mainCamera.GetComponent<SwitchCamera>().Switch();
                    }
                }
            }
        }

        if (gameManager.isGameEnded && spectatingText != null && spectatingText.activeSelf)
            spectatingText.SetActive(false);
    }

    /// <summary>
    /// Respawns the player at the specified location
    /// </summary>
    private void respawn(Vector3 location)
    {
        // Stop all movement
        rigidBody.velocity = Vector3.zero;
        rigidBody.angularVelocity = Vector3.zero;

        // Move to respawn location
        transform.position = location;
    }
}
