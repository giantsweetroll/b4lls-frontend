using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDirection : MonoBehaviour
{
    [SerializeField] public Transform player;
    [SerializeField] public Rigidbody playerRigidBody;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void LateUpdate()
    {   
        // Destroy itself if it's not attached to anything
        if (player == null) {
            Destroy(gameObject);
            return;
        }
        // Keep the direction indicator below the player
        transform.position = new Vector3(
            player.position.x,
            player.position.y - player.localScale.y / 2 + 0.01f,
            player.position.z
        );

        // Rotate the indicator according to the resulting velocity vector of the player's movement.
        if (playerRigidBody.angularVelocity != Vector3.zero)
        {
            transform.rotation = Quaternion.LookRotation(playerRigidBody.angularVelocity);
        }
        transform.rotation = Quaternion.Euler(
            0,
            transform.rotation.eulerAngles.y,
            0
        );
    }
}
