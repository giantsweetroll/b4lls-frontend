using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ramp : MonoBehaviour
{
    public float addedForce = 10.0f;

    private void OnCollisionEnter(Collision collision)
    {
        GameObject obj = collision.gameObject;

        if (obj.tag == "Player")
        {
            // Disable speed limit
            PlayerMovement movement = obj.GetComponent<PlayerMovement>();
            movement.enforceSpeedLimit = false;

            // Add force
            Rigidbody rigidBody = obj.GetComponent<Rigidbody>();
            Vector3 force = (Input.GetAxis("Horizontal") * Camera.main.transform.right * addedForce) + (Input.GetAxis("Vertical") * Camera.main.transform.forward * addedForce);
            rigidBody.AddForce(force, ForceMode.Force);
        }
    }
}
