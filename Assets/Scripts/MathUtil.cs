using UnityEngine;

public class MathUtil {
  public static float cubicEaseOut(float x) {
    return 1f - Mathf.Pow(1f - x, 3);
  }
}