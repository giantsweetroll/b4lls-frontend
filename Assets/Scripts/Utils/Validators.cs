using UnityEngine;

public class Validators : MonoBehaviour
{
    public static bool validateRoomCode(string roomCode)
    {
        return !roomCode.Equals("");
    }
}
