using UnityEngine;

public class Utilities
{
   public static float getHighestVectorRatio(Vector3 current, Vector3 max)
    {
        // Get highest vector
        int type = 0;       // 0, 1, 2 --> x, y, z
        float highest = current.x;
        if (current.y > highest)
        {
            type = 1;
            highest = current.y;
        }

        if (current.z > highest)
        {
            type = 2;
            highest = current.z;
        }

        // Get ratio
        float ratio = 0f;
        switch (type)
        {
            case 0:
                ratio = highest / max.x;
                break;

            case 1:
                ratio = highest / max.y;
                break;

            case 2:
                ratio = highest / max.z;
                break;

            default:
                break;
        }

        return ratio;
    }
}
