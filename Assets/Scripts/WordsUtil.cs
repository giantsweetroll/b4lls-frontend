using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class WordsUtil
{
    static readonly private string[] englishWords = ((TextAsset)Resources.Load("EngWords")).text.Split("\n");

    public static string RandomWordCombination(int num)
    {
        string res = "";
        for (int i = 0; i < num; i++)
        {
            string word = englishWords[Random.Range(0, englishWords.Length)];
            res += char.ToUpper(word[0]) + word[1..];
        }
        return res;
    }
}
