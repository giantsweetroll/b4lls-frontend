using UnityEngine;

public class PlayerCustomizationButtonDelegate : MonoBehaviour
{
    [SerializeField] private GameObject obj;

    private PlayerCustomization playerCustomization;

    private void Start()
    {
        playerCustomization = obj.GetComponent<PlayerCustomization>();
    }

    public void pressPrevius()
    {
        playerCustomization.PressBack();
    }

    public void pressNext()
    {
        playerCustomization.PressNext();
    }
}
