using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class EndgamePanel : MonoBehaviour
{
    public Player winningPlayer;

    [SerializeField] private TMP_Text playerNameText;
    [SerializeField] private Image playerImage;

    private void OnEnable()
    {
        if (winningPlayer != null)
        {
            playerNameText.text = winningPlayer.name;
            // TODO: Set player image
        }
    }

    public void ReturnToMainMenu()
    {
        // TODO: Return to main menu
    }
}
