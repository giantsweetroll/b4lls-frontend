using UnityEngine;
using UnityEngine.UI;
using TMPro; 

public class JoinRoom : MonoBehaviour
{
    public TMP_InputField roomCodeField;
    public UsernameDialog usernameDialog;

    // Wll be called if the user clicks Join Room
    public void ShowUsernameDialog()
    {
        if (!roomCodeField.text.Equals(""))
        {
            usernameDialog.ShowDialog(false);
        }
    }
}
