using UnityEngine;
using UnityEngine.Events;
using TMPro;

public class InputFieldValidation : MonoBehaviour
{

    public delegate bool Validate(string text);
    public delegate void OnSuccessValidation();

    public TMP_InputField inputField;
    public UnityEvent events;
    public Validate validate;
    public OnSuccessValidation onSuccessValidation;

    public void OnValidate()
    {
        if (inputField != null)
        {
            string text = inputField.text;
            if (validate(text))
            {
                onSuccessValidation();
            }
        }
    }
}
