using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UsernameDisplay : MonoBehaviour
{
    public Color color;

    public TMP_Text nameText;
    [SerializeField] private GameObject panelBackground;

    private void Start() {
        // TODO: Evil
        color = Random.ColorHSV();
    }

    public void UpdatePlayer(string name)
    {
        if (name != "")
        {
            nameText.text = name;
            nameText.color = color;
        }
        else
        {
            panelBackground.SetActive(false);
            nameText.text = "Empty";
            nameText.color = new Color(255, 255, 255);
        }
    }
}
