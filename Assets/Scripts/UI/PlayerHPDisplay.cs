using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PlayerHPDisplay : MonoBehaviour
{
    public Player player;
    public Image hp1, hp2, hp3;
    public TMP_Text nameText;
    private Sprite heartEmpty;

    private int curPlayerHP;

    private void Start()
    {
        if (player != null)
        {
            nameText.text = player.name;
            nameText.color = player.color;
            heartEmpty = Resources.Load<Sprite>("Textures/heart_empty");
            curPlayerHP = player.lives;
        }
        else
        {
            gameObject.SetActive(false);
        }
        
    }

    public void UpdateLife() {
        nameText.text = player.name;
        nameText.color = player.color;
        heartEmpty = Resources.Load<Sprite>("Textures/heart_empty");
        curPlayerHP = player.lives;
    }

    // Update is called once per frame
    void Update()
    {
        if (player!= null && player.lives != curPlayerHP)
        {
            curPlayerHP = player.lives;
            UpdateLivesDisplay();
        }
    }

    private void UpdateLivesDisplay()
    {
        switch (curPlayerHP)
        {
            case 2:
                hp3.sprite = heartEmpty;
                break;

            case 1:
                hp2.sprite = heartEmpty;
                break;

            case 0:
                hp1.sprite = heartEmpty;
                break;

            default:
                break;
        }
    }
}
