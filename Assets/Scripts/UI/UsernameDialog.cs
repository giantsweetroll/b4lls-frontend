using UnityEngine;

public class UsernameDialog : MonoBehaviour
{
    public GameObject usernameDialog;
    [SerializeField]
    private PUNInitializer netInitializer;

    public void ShowDialog(bool isCreateRoom)
    {
        netInitializer.shouldCreateRoom = isCreateRoom;
        usernameDialog.SetActive(true);
    }

    public void HideDialog()
    {
        usernameDialog.SetActive(false);
    }

    public void QuitGame() {
        Application.Quit();
    }
}
