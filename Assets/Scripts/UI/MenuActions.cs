using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuActions : MonoBehaviour
{
    [SerializeField] private GameObject menu;

    public void ContinueGame()
    {
        menu.SetActive(false);
        Cursor.lockState = CursorLockMode.Locked;
    }

    public void Quit()
    {
        // TODO: Quit match
    }
}
