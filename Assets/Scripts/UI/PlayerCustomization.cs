using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Networking;
using System.Collections;

public class PlayerCustomization : MonoBehaviour
{

    private class Urls {
        public string[] url;
    }

    private List<string> playerSkinUrls = new List<string>();
    public static List<Sprite> spriteList = new List<Sprite>();
    private int currentLoadedImageIndex = -1;
    private int sizeOfList;
    private int i;
    [SerializeField] private Image skinImage;
    [SerializeField] private TMP_Text skinNameText;

    private Urls urls;
    
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(GetRequest("https://b4llsb4lls-ilmdhmcdwq-et.a.run.app/"));
    }
    IEnumerator DownloadImage(string MediaUrl)
    {
        UnityWebRequest request = UnityWebRequestTexture.GetTexture(MediaUrl);
        yield return request.SendWebRequest();
        if (request.result != UnityWebRequest.Result.Success)
            Debug.Log(request.error);
        else
        {
            Texture2D texture = ((DownloadHandlerTexture)request.downloadHandler).texture;
            Rect rec = new Rect(0, 0, texture.width, texture.height);
            Sprite.Create(texture, rec, new Vector2(0, 0), 1);
            currentLoadedImageIndex++;
            spriteList.Insert(currentLoadedImageIndex, Sprite.Create(texture, rec, new Vector2(0, 0), .01f));
          }
            
    }
    IEnumerator DownloadImageAssign(string MediaUrl)
    {
        UnityWebRequest request = UnityWebRequestTexture.GetTexture(MediaUrl);
        yield return request.SendWebRequest();
        if (request.result != UnityWebRequest.Result.Success)
            Debug.Log(request.error);
        else
        {

            Texture2D texture = ((DownloadHandlerTexture)request.downloadHandler).texture;
            Rect rec = new Rect(0, 0, texture.width, texture.height);
            Sprite.Create(texture, rec, new Vector2(0, 0), 1);
            Sprite initialSprite = Sprite.Create(texture, rec, new Vector2(0, 0), .01f);
            currentLoadedImageIndex++;
            spriteList.Insert(currentLoadedImageIndex, initialSprite);
            skinImage.sprite = initialSprite;
            skinNameText.text = "Skin" + " " + (currentLoadedImageIndex+1);
            i = currentLoadedImageIndex;
        }
        Player.skin = spriteList[i];
    }
    IEnumerator GetRequest(string uri)
    {
        using (UnityWebRequest webRequest = UnityWebRequest.Get(uri))
        {
            // Request and wait for the desired page.
            yield return webRequest.SendWebRequest();
            urls = JsonUtility.FromJson<Urls>(webRequest.downloadHandler.text);
            Debug.Log("Yesso" + webRequest.downloadHandler.text);
            OnFinishFetching();
        }
    }

    private void OnFinishFetching() {
        int i = 0;
        foreach (string url in urls.url) {
            playerSkinUrls.Add(url);
            if (i == 0) {
                StartCoroutine(DownloadImageAssign(url));
            } else {
                StartCoroutine(DownloadImage(url));
            }
            i++;
        }

        sizeOfList = playerSkinUrls.Count;
    }

    public void PressBack()
    {
        i -= 1;
        if (i < 0){
            i = sizeOfList-1;
        }
        skinImage.sprite = spriteList[i];
        skinNameText.text = "Skin" + " " + (i + 1);

        Player.skin = spriteList[i];
    }

    public void PressNext()
    {
        i += 1;
        if (i >= sizeOfList)
        {
            i = 0;
        }
        
        skinImage.sprite = spriteList[i];
        skinNameText.text = "Skin" + " "+ (i+1);

        Player.skin = spriteList[i];
    }

    public Sprite GetChosenSkin() {
        return spriteList[i];
    }
}
