using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Ground : MonoBehaviour
{
    [SerializeField] private GameObject shrinkingGroundMessage;

    [SerializeField] [Tooltip("Whether to allow the ground to shrink")] private bool canShrink = false;
    [SerializeField] [Tooltip("How fast the ground shrinks in every step")] private float shrinkRate = 0.5f;
    [SerializeField] [Tooltip("Controls how often the ground shrinks")] private float shrinkEverySeconds = 1;
    [SerializeField] [Tooltip("Amount to shrink in each step")] private Vector3 shrinkStepScale = new Vector3(10, 10, 10);
    [HideInInspector] public bool isShrinking = false;
    [Tooltip("How many times should the ground shrinks")] public int shrinkSteps = 3;
    [HideInInspector] public int currentShrinkStep = 0;
    private Vector3 nextScaleMilestone;     // Target scale of the ground for the next step
    [SerializeField]
    [Tooltip("Shrinking ground audio to be played when ground shrinks. Audio needs to be marked as looping.")]
    private AudioSource shrinkingAudio;

    private void Start()
    {
        nextScaleMilestone = transform.localScale - shrinkStepScale;
        if (canShrink)
        {
            print(nextScaleMilestone);
        }
        Invoke(nameof(StartShrink), shrinkEverySeconds);
    }

    private void StartShrink()
    {
        if (canShrink)
        {
            if (shrinkingAudio != null)
                shrinkingAudio.Play();

            currentShrinkStep++;
            isShrinking = true;

            if (shrinkingGroundMessage != null)
                shrinkingGroundMessage.SetActive(true);

            print("Start shrinking");
        }
    }

    private void Update()
    {
        if (isShrinking && canShrink && currentShrinkStep <= shrinkSteps)
        {
            //print("Ground shrinking");
            // The proper shrink rate adjusted to the framerate
            float trueShrinkRate = shrinkRate * Time.deltaTime;
            // Update the ground's scaling
            transform.localScale = new Vector3(
                transform.localScale.x - trueShrinkRate < nextScaleMilestone.x ? nextScaleMilestone.x : transform.localScale.x - trueShrinkRate,
                transform.localScale.y,
                transform.localScale.z - trueShrinkRate < nextScaleMilestone.z ? nextScaleMilestone.z : transform.localScale.z - trueShrinkRate
            );

            /**
            print("Local Scale:");
            print(transform.localScale);
            print("Next scale Milestone:");
            print(nextScaleMilestone);
             **/
            if (transform.localScale.x <= nextScaleMilestone.x &&
                transform.localScale.z <= nextScaleMilestone.z
            )
            {
                nextScaleMilestone = transform.localScale - shrinkStepScale;

                if (shrinkingAudio != null)
                {
                    shrinkingAudio.Stop();
                }

                isShrinking = false;

                Invoke(nameof(StartShrink), shrinkEverySeconds);
            }
        } 
        else 
        {
            if (shrinkingAudio != null)
                shrinkingAudio.Stop();
            if (shrinkingGroundMessage != null)
                shrinkingGroundMessage.SetActive(false);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        GameObject obj = collision.gameObject;

        if (obj.tag == "Player")
        {
            PlayerMovement movement = obj.GetComponent<PlayerMovement>();

            Player player = obj.GetComponent<Player>();

            //Play bounce sfx
            if (!player.isOnSticky)
            {
                AudioSource bounceAudio = player.audioBounce;
                bounceAudio.Play();
            }

            // Enable speed limit
            movement.enforceSpeedLimit = true;
        }
    }

    private void OnCollisionStay(Collision collision)
    {
        GameObject obj = collision.gameObject;

        if (obj.tag == "Player")
        {
            PlayerMovement movement = obj.GetComponent<PlayerMovement>();
            Player player = obj.GetComponent<Player>();

            // Allow jump
            movement.canJump = !player.isOnSticky;

            if (!player.isOnSticky)
            {
                // Play ground rolling audio as long as the ball is moving
                Rigidbody rb = obj.GetComponent<Rigidbody>();
                if (rb.velocity != Vector3.zero || rb.angularVelocity != Vector3.zero)
                {
                    AudioSource groundRollingAudio = player.audioGroundRoll;

                    groundRollingAudio.volume = Utilities.getHighestVectorRatio(rb.angularVelocity, movement.maxVelocity);

                    if (!groundRollingAudio.isPlaying && rb.angularVelocity != Vector3.zero)
                    {
                        groundRollingAudio.Play();
                    }
                }
            }
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        GameObject obj = collision.gameObject;

        if (obj.tag == "Player")
        {
            // Disable speed limit
            PlayerMovement movement = obj.GetComponent<PlayerMovement>();
            movement.enforceSpeedLimit = false;

            // Disallow jump
            movement.canJump = false;

            // Stop playing ground rolling audio
            Player player = obj.GetComponent<Player>();
            Rigidbody rb = obj.GetComponent<Rigidbody>();
            AudioSource groundRollingAudio = player.audioGroundRoll;
            if (groundRollingAudio.isPlaying)
            {
                groundRollingAudio.Stop();
            }
        }
    }
}
